import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.scss";
import {
  Navigate,
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";
import ErrorPage from "./views/error/Error.tsx";
import { Home } from "./views/home/Home.tsx";
import { Spots } from "./views/spots/Spots.tsx";
import { Shop } from "./views/shop/Shop.tsx";
import { About } from "./views/about/About.tsx";
import { Contact } from "./views/contact/Contact.tsx";
import { Account } from "./views/account/Account.tsx";
import { Help } from "./views/help/Help.tsx";
import { Dashboard } from "./components/dashboard/Dashboard.tsx";
import { Orders } from "./components/orders/Orders.tsx";
import { Details } from "./components/details/Details.tsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/spots",
        element: <Spots />,
      },
      {
        path: "/shop",
        element: <Shop />,
      },
      {
        path: "/about",
        element: <About />,
      },
      {
        path: "/contact",
        element: <Contact />,
      },
      {
        path: "/account",
        element: <Account />,
        children: [
          {
            path: "/account/",
            element: <Navigate to={"/account/dashboard"} />,
          },
          {
            path: "/account/dashboard",
            element: <Dashboard />,
          },
          {
            path: "/account/orders",
            element: <Orders />,
          },
          {
            path: "/account/details",
            element: <Details />,
          },
          {
            path: "/account/logout",
            element: <Navigate to={"/"} />,
          },
          {
            path: "*",
            element: <Navigate to={"/account/dashboard"} />,
          },
        ],
      },
      {
        path: "/help",
        element: <Help />,
      },
      {
        path: "*",
        element: <Navigate to={"/"} />,
      },
    ],
  },
  {
    path: "*",
    element: <Navigate to={"/"} />,
  },
]);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
