import { Card } from "../../components/card/Card";
import "./Shop.scss";
import data from "../../assets/data/shop.json";
import { ShopType } from "../../types/Shop.type";

export const Shop = () => {
  return (
    <section className="shop">
      <h1>hiking shop</h1>
      <p>Gear up for your next adventure</p>
      <section className="articles">
        {data.map((shopItem: ShopType) => (
          <Card
            id={shopItem.id}
            name={shopItem.name}
            price={shopItem.price}
            img={shopItem.img}
          />
        ))}
      </section>
    </section>
  );
};
