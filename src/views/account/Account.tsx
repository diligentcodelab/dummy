import { NavLink, Outlet } from "react-router-dom";
import "./Account.scss";

export const Account = () => {
  return (
    <section className="account">
      <h1>my account</h1>
      <section className="main-section">
        <aside>
          <ul>
            <NavLink to={"/account/dashboard"}>
              <li>
                <h3>dashboard</h3>
              </li>
            </NavLink>
            <NavLink to={"/account/orders"}>
              <li>
                <h3>orders</h3>
              </li>
            </NavLink>
            <NavLink to={"/account/logout"}>
              <li>
                <h3>logout</h3>
              </li>
            </NavLink>
          </ul>
        </aside>
        <article className="content">
          <Outlet />
        </article>
      </section>
    </section>
  );
};
