import { Map } from "../../components/map/Map";
import "./Spots.scss";

export const Spots = () => {
  return (
    <section className="spots">
      <h1>best hiking spots</h1>
      <p>Where adventure meets serenity: discover the best hiking spots</p>
      <article className="map">
        <Map />
      </article>
    </section>
  );
};
