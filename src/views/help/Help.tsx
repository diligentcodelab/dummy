import "./Help.scss";

export const Help = () => {
  return (
    <section className="help">
      <h1>Frequenlty asked questions</h1>
      <ul>
        <li>
          <h3>What makes Hikers unique?</h3>
          <p>
            At Hikers, we're not just a store; we're a community of nature
            enthusiasts dedicated to promoting the joy of hiking. Our commitment
            to quality gear, sustainable practices, and a love for the great
            outdoors sets us apart. We curate products that inspire and empower,
            ensuring every hiker finds the perfect companion for their journey.
          </p>
        </li>
        <li>
          <h3>How do I choose the right gear for my hike?</h3>
          <p>
            We understand that selecting the right gear is crucial for a
            successful hike. Our website features detailed product descriptions
            and guides to help you make informed choices. If you need
            personalized assistance, our customer service team is always ready
            to offer expert advice and recommendations.
          </p>
        </li>
        <li>
          <h3>Are your products eco-friendly?</h3>
          <p>
            Yes, we are committed to sustainability. We prioritize products from
            brands that share our dedication to environmentally conscious
            practices. Look for our eco-friendly tag on select items, indicating
            a commitment to minimizing our ecological footprint.
          </p>
        </li>
        <li>
          <h3>Can I return or exchange my purchase?</h3>
          <p>
            Absolutely. We want you to be completely satisfied with your
            purchase. Please refer to our Returns & Exchanges policy for
            detailed information on the process, timelines, and any eligibility
            criteria.
          </p>
        </li>
        <li>
          <h3>How do I track my order?</h3>
          <p>
            Once your order is shipped, you will receive a confirmation email
            with tracking information. You can easily track your package through
            the provided link. If you encounter any issues, our customer service
            team is here to assist you.
          </p>
        </li>
        <li>
          <h3>Do you ship internationally?</h3>
          <p>
            Yes, we offer international shipping to bring the joy of hiking to
            enthusiasts around the world. Please check our Shipping Information
            page for details on delivery times and costs.
          </p>
        </li>
        <li>
          <h3>How can I stay updated on Hikers' promotions and events?</h3>
          <p>
            Stay connected with us! Subscribe to our newsletter for the latest
            updates on promotions, new arrivals, and upcoming events. Follow us
            on social media platforms for a dose of inspiration, community
            highlights, and exclusive content.
          </p>
        </li>
        <li>
          <h3>How can I get involved in Hikers' community initiatives?</h3>
          <p>
            We love active participants! Keep an eye on our Community page for
            information on events, clean-up initiatives, and collaborations.
            Join us in making a positive impact on the trails we cherish.
          </p>
        </li>
        <h3>
          For any other inquiries, feel free to contact our customer service
          team. <br />
          <br />
          Happy hiking!
        </h3>
      </ul>
    </section>
  );
};
