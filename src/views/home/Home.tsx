import "./Home.scss";

export const Home = () => {
  return (
    <section className="home">
      <h1 className="brand-name">hikers</h1>
      <p className="tagline">Exploring nature, one step at a time</p>
    </section>
  );
};
