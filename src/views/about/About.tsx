import "./About.scss";

export const About = () => {
  return (
    <section className="about">
      <h1>who are we</h1>
      <h3>Welcome to Hikers – Where Nature Beckons and Adventure Awaits!</h3>
      <p>
        At Hikers, we believe in the transformative power of hiking and the
        profound connection it forges between individuals and the great
        outdoors.
        <br />
        <br />
        Our passion is to inspire a love for nature and exploration, encouraging
        everyone to step outside, breathe in the fresh air, and embark on
        unforgettable journeys through diverse landscapes.
        <br />
        <br />
        Founded with a spirit of adventure, Hikers is more than just a brand;
        it's a community united by the shared love for hiking and the boundless
        wonders of the natural world. We understand that every step taken on the
        trail is a step toward self-discovery, wellness, and a deeper
        appreciation for the beauty that surrounds us.
        <br />
        <br />
        What sets Hikers apart is our commitment to providing high-quality gear
        and resources that empower outdoor enthusiasts of all levels. From
        seasoned trailblazers to those taking their first steps into the
        wilderness, we curate a selection of top-notch hiking equipment,
        apparel, and accessories to ensure every adventure is met with
        confidence and comfort.
        <br />
        <br />
        Our ethos goes beyond commerce; it's a celebration of the awe-inspiring
        landscapes that make our planet extraordinary. We are advocates for
        responsible and sustainable hiking, fostering a deep respect for the
        environments we explore. Through partnerships with conservation
        organizations, we strive to give back to the trails and natural spaces
        that have given us so much joy.
        <br />
        <br />
        Join us on this journey of discovery, where every trail tells a story
        and every summit offers a new perspective. Hikers is more than a brand –
        it's an invitation to embrace the call of the wild and experience the
        exhilaration of hiking firsthand.
      </p>
      <h3>Step into nature. Adventure awaits. Welcome to Hikers.</h3>
    </section>
  );
};
