import "./Map.scss";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import GR from "../../assets/data/gr.json";
import { GRType } from "../../types/Gr.type";
import { LatLngExpression } from "leaflet";

export const Map = () => {
  return (
    <MapContainer center={[46.749198, 2.413086]} zoom={6}>
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {GR.map((gr: GRType) => (
        <>
          <Marker position={gr.coordinates[0] as LatLngExpression}>
            <Popup>
              <h2>{gr.ref}</h2>
              <h3>{gr.name}</h3>
              <p>{`${gr.distance} kms`}</p>
            </Popup>
          </Marker>
          <Marker position={gr.coordinates[1] as LatLngExpression}>
            <Popup>
              <h2>{gr.ref}</h2>
              <h3>{gr.name}</h3>
              <p>{`${gr.distance} kms`}</p>
            </Popup>
          </Marker>
        </>
      ))}
    </MapContainer>
  );
};
