import { NavLink } from "react-router-dom";
import "./Navbar.scss";
import logo from "../../assets/logo.png";

export const Navbar = () => {
  return (
    <header className="header">
      <nav className="navbar">
        <NavLink to={"/"}>
          <div className="logo">
            <img
              src={logo}
              height={60}
              title="Hikers brand logo"
              alt="Hikers brand logo presenting a hiker in the foreground with trees and mountains in the background"
              loading="lazy"
            />
          </div>
        </NavLink>
        <div className="menu">
          <ul>
            <li>
              <NavLink to={"/spots"}>hiking spots</NavLink>
            </li>
            <li>
              <NavLink to={"/shop"}>hiking shop</NavLink>
            </li>
            <li>
              <NavLink to={"/about"}>about hikers</NavLink>
            </li>
            <li>
              <NavLink to={"/contact"}>contact us</NavLink>
            </li>
          </ul>
        </div>
        <div className="menu-right">
          <ul>
            <li>
              <NavLink to={"/account"}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 448 512"
                  fill="#fafcf7"
                  height={15}
                  className="svg"
                >
                  <path d="M224 256A128 128 0 1 0 224 0a128 128 0 1 0 0 256zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z" />
                </svg>
              </NavLink>
            </li>
            <li>
              <NavLink to={"/help"}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 320 512"
                  fill="#fafcf7"
                  height={15}
                  className="svg"
                >
                  <path d="M80 160c0-35.3 28.7-64 64-64h32c35.3 0 64 28.7 64 64v3.6c0 21.8-11.1 42.1-29.4 53.8l-42.2 27.1c-25.2 16.2-40.4 44.1-40.4 74V320c0 17.7 14.3 32 32 32s32-14.3 32-32v-1.4c0-8.2 4.2-15.8 11-20.2l42.2-27.1c36.6-23.6 58.8-64.1 58.8-107.7V160c0-70.7-57.3-128-128-128H144C73.3 32 16 89.3 16 160c0 17.7 14.3 32 32 32s32-14.3 32-32zm80 320a40 40 0 1 0 0-80 40 40 0 1 0 0 80z" />
                </svg>
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  );
};
