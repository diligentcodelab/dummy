import { useEffect, useState } from "react";
import "./SvgBar.scss";
import svgJson from "../../assets/data/svg.json";

export const SvgBar = () => {
  const [data, setData] = useState<
    { name: string; percent: number; color: string }[]
  >([]);

  useEffect(() => {
    setData(svgJson);
  }, []);

  const generateBars = (
    d: { name: string; percent: number; color: string },
    index: number
  ) => {
    let startingPoint = 0;
    for (let i = 0; i < index; i++) {
      startingPoint += data[i].percent;
    }

    const endPoint = ((startingPoint + d.percent) / 100) * 400;
    startingPoint = (startingPoint / 100) * 400;

    return (
      <line
        x1={startingPoint}
        y1="100"
        x2={endPoint}
        y2="100"
        stroke={d.color}
        strokeWidth={10}
        strokeDasharray={1000}
        className="path"
      >
        <title>
          {d.name} - {d.percent}%
        </title>
      </line>
    );
  };

  return (
    <svg width={400} height={200} className="bar">
      <text
        x={200}
        y={50}
        textAnchor="middle"
        className="svg-text"
        fill="#fafcf7"
      >
        Objectives (90% reached)
      </text>
      <line
        x1="0"
        y1="100"
        x2="400"
        y2="100"
        stroke={"#fafcf7"}
        strokeWidth={2}
      />
      {data.map(
        (d: { name: string; percent: number; color: string }, index: number) =>
          generateBars(d, index)
      )}
    </svg>
  );
};
