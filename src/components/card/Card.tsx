import "./Card.scss";
import { ShopType } from "../../types/Shop.type";

export const Card = (item: ShopType) => {
  return (
    <article className="container">
      <figure className="card">
        <img className="image" src={`./assets/data/img/${item.img}`} />
        <figcaption className="text">
          <h2>{item.name}</h2>
          <p>{`${item.price.toFixed(2)} €`}</p>
        </figcaption>
      </figure>
    </article>
  );
};
