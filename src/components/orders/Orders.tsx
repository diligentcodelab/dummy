import "./Orders.scss";
import orders from "../../assets/data/orders.json";
import shop from "../../assets/data/shop.json";
import { OrderArticleType, OrderType } from "../../types/Order";
import { ShopType } from "../../types/Shop.type";

export const Orders = () => {
  return (
    <table className="orders">
      <thead>
        <tr>
          <th>order number</th>
          <th>date</th>
          <th></th>
          <th>article</th>
          <th>price</th>
          <th>quantity</th>
          <th>total price</th>
        </tr>
      </thead>
      <tbody>
        {orders.map((order: OrderType) =>
          order.articles.map((article: OrderArticleType, index: number) => {
            const selected_article = shop.find(
              (item: ShopType) => item.id === article.id
            );
            if (selected_article) {
              if (index === 0) {
                return (
                  <>
                    <tr className="separator">
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr className="order">
                      <td rowSpan={order.articles.length}>
                        {order.order_number}
                      </td>
                      <td rowSpan={order.articles.length}>
                        {order.order_date}
                      </td>
                      <td>
                        <img
                          height={60}
                          src={`/assets/data/img/${selected_article.img}`}
                        />
                      </td>
                      <td className="name">{selected_article.name}</td>
                      <td className="price">
                        {selected_article.price.toFixed(2)} €
                      </td>
                      <td className="quantity">{article.quantity}</td>
                      <td rowSpan={order.articles.length}>
                        {order.total.toFixed(2)} €
                      </td>
                    </tr>
                  </>
                );
              } else {
                return (
                  <tr className="order">
                    <td>
                      <img
                        height={60}
                        src={`/assets/data/img/${selected_article.img}`}
                      />
                    </td>
                    <td className="name">{selected_article.name}</td>
                    <td className="price">
                      {selected_article.price.toFixed(2)} €
                    </td>
                    <td className="quantity">{article.quantity}</td>
                  </tr>
                );
              }
            } else {
              return;
            }
          })
        )}
      </tbody>
    </table>
  );
};
