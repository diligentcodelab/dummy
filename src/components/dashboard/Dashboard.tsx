import { SvgBar } from "../svg-bar/SvgBar";
import { SvgDonut } from "../svg-donut/SvgDonut";
import "./Dashboard.scss";

export const Dashboard = () => {
  return (
    <section className="dashboard">
      <SvgDonut donutSize={200} />
      <SvgBar />
    </section>
  );
};
