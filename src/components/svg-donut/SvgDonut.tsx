import { useEffect, useState } from "react";
import "./SvgDonut.scss";
import svgJson from "../../assets/data/svg.json";

export const SvgDonut = ({ donutSize }: { donutSize: number }) => {
  const [data, setData] = useState<
    { name: string; percent: number; color: string }[]
  >([]);

  useEffect(() => {
    setData(svgJson);
  }, []);

  const translatedPoint = (
    center: { x: number; y: number },
    radius: number,
    degrees: number
  ) => {
    const radians = (Math.PI * (degrees - 90)) / 180;
    return {
      x: center.x + radius * Math.cos(radians),
      y: center.y + radius * Math.sin(radians),
    };
  };

  const buildArcPath = (
    center: { x: number; y: number },
    radius: number,
    index: number
  ) => {
    let degreesStart = 0;

    for (let i = 0; i < index; i++) {
      degreesStart += data[i].percent;
    }

    const degreesEnd = ((degreesStart + data[index].percent) / 100) * 360;
    degreesStart = (degreesStart / 100) * 360;

    const start = translatedPoint(center, radius, degreesStart),
      end = translatedPoint(center, radius, degreesEnd),
      largeArc = degreesEnd - degreesStart > 180 ? 1 : 0;

    return `M${start.x},${start.y} A${radius},${radius} 0 ${largeArc},1 ${end.x},${end.y}`;
  };

  return (
    <svg width={400} height={200}>
      <pattern id="scribbles" viewBox="0 0 10 10" width={"10%"} height={"10%"}>
        <path
          fill={"none"}
          stroke={"#fafcf7"}
          strokeWidth={5}
          d="M10-5-10,15M15,0,0,15M0-5-20,15"
        ></path>
      </pattern>
      <mask id="circle-mask">
        <circle
          cx={donutSize / 2}
          cy={donutSize / 2}
          r={(donutSize * 0.8) / 2}
          fill="url(#scribbles)"
        ></circle>
      </mask>
      <circle
        cx={donutSize / 2}
        cy={donutSize / 2}
        r={(donutSize * 0.8) / 2}
        stroke={"#fafcf7"}
        fill="#fafcf7"
        mask="url(#circle-mask)"
        strokeWidth={2}
        opacity={0.1}
      ></circle>
      <circle
        cx={donutSize / 2}
        cy={donutSize / 2}
        r={(donutSize * 0.8) / 2}
        stroke={"#fafcf7"}
        fill="none"
        strokeWidth={1}
      ></circle>
      {data.map(
        (
          d: { name: string; percent: number; color: string },
          index: number
        ) => (
          <>
            <circle
              cx={donutSize / 2 + (donutSize * 0.8) / 2 + 50}
              cy={
                donutSize / 2 -
                (data.length / 2) * 30 +
                (donutSize * 0.8) / 4 +
                (30 - (donutSize * 0.8) / 2) +
                30 * (index + 1)
              }
              r={10}
              stroke={"#fafcf7"}
              fill={d.color}
              strokeWidth={2}
            ></circle>
            <text
              x={donutSize / 2 + (donutSize * 0.8) / 2 + 80}
              y={
                donutSize / 2 -
                (data.length / 2) * 30 +
                (donutSize * 0.8) / 4 +
                (30 - (donutSize * 0.8) / 2) +
                30 * (index + 1) +
                5
              }
              fill={"#fafcf7"}
              className="svg-text"
            >
              {d.name}
            </text>
            <path
              d={buildArcPath(
                { x: donutSize / 2, y: donutSize / 2 },
                (donutSize * 0.8) / 2,
                index
              )}
              fill="none"
              strokeWidth={10}
              stroke={d.color}
              strokeDasharray={1000}
              className="path"
            >
              <title>
                {d.name} - {d.percent}%
              </title>
            </path>
          </>
        )
      )}
    </svg>
  );
};
