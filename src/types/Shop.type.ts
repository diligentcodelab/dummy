export interface ShopType {
  id: number;
  name: string;
  price: number;
  img: string;
}
