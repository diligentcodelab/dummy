export interface GRType {
  id: number;
  name: string;
  ref: string;
  to: string;
  from: string;
  description: string;
  distance: number;
  coordinates: number[][];
}
