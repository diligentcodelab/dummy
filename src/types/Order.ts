export interface OrderType {
  order_number: number;
  order_date: string;
  articles: OrderArticleType[];
  total: number;
}

export interface OrderArticleType {
  id: number;
  quantity: number;
}
